package homework3

fun main() {
    //write your code here
    val lemanade: Int = 18500
    val pinacolada: Short = 200
    val whiskey: Byte = 50
    val fresh: Long = 3000000000
    val cola: Float = 0.5F
    val ale: Double = 0.666666667
    val something: String = "\"Что-то авторское!\""
    println("Заказ '$lemanade мл лимонада' готов!")
    println("Заказ '$pinacolada мл пина колады' готов!")
    println("Заказ '$whiskey мл виски' готов!")
    println("Заказ '$fresh капель фреша' готов!")
    println("Заказ '$cola литра колы' готов!")
    println("Заказ '$ale литра эля' готов!")
    println("Заказ '$something' готов!")
}