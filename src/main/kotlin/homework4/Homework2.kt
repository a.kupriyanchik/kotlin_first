package homework4

fun main() {
    val marks = arrayOf(3, 4, 5, 2, 3, 5, 5, 2, 4, 5, 2, 4, 5, 3, 4, 3, 3, 4, 4, 5)
    //write your code here
    var studyingGrade5 = 0
    var studyingGrade4 = 0
    var studyingGrade3 = 0
    var studyingGrade2 = 0
    for (element in marks){
        when (element) {
            5 -> studyingGrade5+=1
            4 -> studyingGrade4+=1
            3 -> studyingGrade3+=1
            2 -> studyingGrade2+=1
        }
    }
    println ("Отличников - ${(studyingGrade5.toDouble()/marks.size) * 100} %.")
    println ("Хорошистов - ${(studyingGrade4.toDouble()/marks.size) * 100} %.")
    println ("Троечников - ${(studyingGrade3.toDouble()/marks.size) * 100} %.")
    println ("Двоечников - ${(studyingGrade2.toDouble()/marks.size) * 100} %.")
}