package homework5
fun main () {
    val mylion = Lion ("Simba", 50, 52)
    val myTiger = Tiger ("Stripy", 48, 45)
    val myHippopotamus = Hippopotamus ("Hippo", 110, 540)
    val myWolf = Wolf ("Alfa", 45, 62)
    val myGiraffe = Giraffe ("Gira", 415, 580)
    val myElephant = Elephant ("Elphy", 250, 1500)
    val myChimpanzee = Chimpanzee ("Chimpy", 85, 68)
    val myGorilla = Gorilla ("KingKong", 250, 420)
    println ("Что едим сегодня?")
    var foodToEat = readLine()!!.toString()
    mylion.eat(foodToEat)
    myTiger.eat (foodToEat)
    myHippopotamus.eat (foodToEat)
    myWolf.eat (foodToEat)
    myGiraffe.eat (foodToEat)
    myElephant.eat (foodToEat)
    myChimpanzee.eat (foodToEat)
    myGorilla.eat (foodToEat)
}

class Lion(val name: String, val height: Int, var weight: Int) {
    var favoriteFood = arrayOf("meat", "candy", "grass")
    var eatEnough = 0
    fun eat(food: String) {
        for (element in favoriteFood) {
            if (element == food)  eatEnough++
        }
        if (eatEnough > 0) println("$name наелся.")
         else println("$name остался голодным.")
    }
}

class Tiger(val name: String, val height: Int, var weight: Int) {
    var favoriteFood = arrayOf("meat", "candy", "Lays")
    var eatEnough = 0
    fun eat(food: String) {
        for (element in favoriteFood) {
            if (element == food) eatEnough++
        }
        if (eatEnough > 0) println("$name наелся.")
        else println("$name остался голодным.")
    }

}

class Hippopotamus(val name: String, val height: Int, var weight: Int) {
    var favoriteFood = arrayOf("grass", "leaves", "bananas")
    var eatEnough = 0
    fun eat(food: String) {
        for (element in favoriteFood) {
            if (element == food) eatEnough++
        }
        if (eatEnough > 0) println("$name наелся.")
        else println("$name остался голодным.")
    }
}

class Wolf(val name: String, val height: Int, var weight: Int) {
    var favoriteFood = arrayOf("meat", "candy", "pizza")
    var eatEnough = 0
    fun eat(food: String) {
        for (element in favoriteFood) {
            if (element == food) eatEnough++
        }
        if (eatEnough > 0) println("$name наелся.")
        else println("$name остался голодным.")
    }
}

class Giraffe(val name: String, val height: Int, var weight: Int) {
    var favoriteFood = arrayOf("leaves", "grass", "apples")
    var eatEnough = 0
    fun eat(food: String) {
        for (element in favoriteFood) {
            if (element == food) eatEnough++
        }
        if (eatEnough > 0) println("$name наелся.")
        else println("$name остался голодным.")
    }
}

class Elephant(val name: String, val height: Int, var weight: Int) {
    var favoriteFood = arrayOf("grass", "apples", "leaves", "bananas")
    var eatEnough = 0
    fun eat(food: String) {
        for (element in favoriteFood) {
            if (element == food) eatEnough++
        }
        if (eatEnough > 0) println("$name наелся.")
        else println("$name остался голодным.")
    }
}

class Chimpanzee(val name: String, val height: Int, var weight: Int) {
    var favoriteFood = arrayOf("bananas", "apples", "Lays")
    var eatEnough = 0
    fun eat(food: String) {
        for (element in favoriteFood) {
            if (element == food) eatEnough++
        }
        if (eatEnough > 0) println("$name наелся.")
        else println("$name остался голодным.")
    }
}

class Gorilla(val name: String, val height: Int, var weight: Int) {
    var favoriteFood = arrayOf("bananas", "apples", "candy")
    var eatEnough = 0
    fun eat(food: String) {
        for (element in favoriteFood)
            if (element == food) eatEnough++
        if (eatEnough > 0) println("$name наелся.")
        else println("$name остался голодным.")
    }
}