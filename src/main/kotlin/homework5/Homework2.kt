package homework5

import kotlin.math.abs

fun main() {
    println("Введите число:")
    val digit = readLine()!!.toInt()
    if (digit < 0) print ("-")
    var y = abs (digit)
    while (y != 0){
        print (y % 10)
        y /= 10
        }
    }
