package homework6
fun main () {
    val myZoo: Array <Animal> = arrayOf(
        Lion("Simba", 50, 52, arrayOf("meat", "candy", "Lays")),
        Tiger("Stripy", 50, 80, arrayOf("meat", "candy")),
        Hippopotamus("Hippo", 250, 1500, arrayOf("grass", "banana", "apple")),
        Wolf("Alfa", 90, 85, arrayOf("meat", "candy")),
        Giraffe("Gira", 500, 1000, arrayOf("grass", "banana","leaves", "apple")),
        Elephant("Elphy", 240, 1800, arrayOf("grass", "banana", "apple")),
        Chimpanzee("Chimpy", 180, 200, arrayOf("banana", "apple", "candy", "Lays")),
        Gorilla ("KingKong", 310, 420, arrayOf("banana", "candy", "Lays", "meat"))
    )
    val food = arrayOf("Lays", "meat", "candy")
    feeding(myZoo, food)
}

fun feeding(animals: Array<Animal>, food: Array<String>) {
        for(animal in animals){
            animal.eat(food)
        }
}

abstract class Animal() {
    abstract val name: String
    abstract val height: Int
    abstract val weight: Int
    abstract var favoriteFood: Array<String>
    open var eatEnough:Int = 0
    open fun eat(food: Array<String>) {
        val smtToEat = favoriteFood.intersect(food.toList().toSet()).toSet()
//        println("$smtToEat")
        if(smtToEat.isNotEmpty()){
            eatEnough += smtToEat.size
            println("$name съел $smtToEat и наелся! Уровень сытости $eatEnough.")
        }
        else println ("$name остался голодным!")
    }
}
class Lion(override val name: String, override val height: Int,override val weight: Int, override var favoriteFood: Array<String>): Animal(){}

class Tiger(override val name: String, override val height: Int, override val weight: Int, override var favoriteFood: Array<String>): Animal(){}

class Hippopotamus(override val name: String, override val height: Int, override val weight: Int, override var favoriteFood: Array<String>): Animal(){}

class Wolf(override val name: String, override val height: Int, override val weight: Int, override var favoriteFood: Array<String>): Animal(){}

class Giraffe(override val name: String, override val height: Int, override val weight: Int, override var favoriteFood: Array<String>): Animal(){}

class Elephant(override val name: String, override val height: Int, override val weight: Int, override var favoriteFood: Array<String>): Animal(){}

class Chimpanzee(override val name: String, override val height: Int, override val weight: Int, override var favoriteFood: Array<String>): Animal(){}

class Gorilla(override val name: String, override val height: Int, override val weight: Int, override var favoriteFood: Array<String>): Animal(){}
