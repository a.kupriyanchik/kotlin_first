package homework7

fun main() {
   val newUser = registration("Ivan","qwertyqwerty", "qwertyqwerty")
//    println (newUser)
}

//write your function and Exception-classes her
fun registration(login: String, password: String, confirmPassword: String):User {
    if (login.length > 20) throw WrongLoginException("Логин должен содержать от 1 до 20 символов!")
    if (password.length < 10) throw WrongPasswordException("Пароль не должен быть короче 10 символов!")
    if (password != confirmPassword) throw WrongPasswordException("Введенные пароли не совпадают!")
 //   val newUser = User (login, password)
    println ("Новый клиент успешно зарегистрирован!")
    return User (login, password)
}

class User(val login: String, val password: String)

class WrongLoginException(override val message: String): Exception()

class WrongPasswordException(override val message: String): Exception()



